<?php



use Illuminate\Support\Facades\Route;
use App\Http\Livewire\BlogIndex;
use App\Http\Livewire\BlogShow;
use App\Http\Livewire\BlogForm;
use App\Http\Livewire\StateIndex;
use App\Http\Livewire\StateShow;

Route::redirect('/register', '/login');

Route::view('/', 'pages.home')->name('home');
Route::view('/about', 'pages.about')->name('about');
Route::view('/services', 'pages.services')->name('services');
Route::view('/process', 'pages.process')->name('process');
Route::view('/locations', 'pages.locations')->name('locations');
Route::view('/faqs', 'pages.faqs')->name('faqs');
Route::view('/privacy-policy', 'pages.privacy-policy')->name('privacy-policy');
Route::view('/nflpa', 'pages.nflpa')->name('nflpa');
Route::view('/quote', 'pages.quote')->name('quote');
Route::view('/quote-nflpa', 'pages.quote-nflpa')->name('quote-nflpa');

Route::middleware(['auth:sanctum', config('jetstream.auth_session'), 'verified'])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});



Route::get('/blog', BlogIndex::class)->name('blog.index');
Route::get('/blog/{slug}', BlogShow::class)->name('blog.show');

Route::get('/states', StateIndex::class)->name('states.index');
Route::get('/state/{slug}', StateShow::class)->name('state.show');



Route::get('/create-blog', BlogForm::class)->name('blog.form');


Route::get('sitemap.xml', 'App\Http\Controllers\SitemapController@index');
