<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_shipping', function (Blueprint $table) {
            $table->text('current_former')->after('is_operable')->nullable();
            $table->text('raider_id')->after('current_former')->nullable();
            $table->text('type_player')->after('raider_id')->nullable();
            $table->date('desired_date')->after('pickup_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_shipping', function (Blueprint $table) {
            $table->dropColumn('current_former');
            $table->dropColumn('raider_id');
            $table->dropColumn('type_player');
            $table->dropColumn('desired_date');
        });
    }
};
