<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_shipping', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('transport_from');
            $table->unsignedInteger('transport_to');
            $table->string('transport_type');
            $table->string('vehicle_year');
            $table->string('vehicle_make');
            $table->string('vehicle_model');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->string('is_operable');
            $table->date('pickup_date');
            $table->timestamps();

            $table->index([ 'name', 'phone']);
            $table->foreign('transport_from')->references('id')->on('cities');
            $table->foreign('transport_to')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_shipping');
    }
};
