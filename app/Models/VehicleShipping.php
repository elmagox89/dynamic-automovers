<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleShipping extends Model
{
    use HasFactory;

    protected $table = "vehicle_shipping";
    protected $fillable = [
        'transport_from',
        'transport_to',
        'transport_type',
        'vehicle_year',
        'vehicle_make',
        'vehicle_model',
        'current_former',
        'raider_id',
        'type_player',
        'name',
        'phone',
        'email',
        'is_operable',
        'pickup_date',
        'desired_date'
    ];


    public function cityFrom(){
        return $this->hasOne(City::class, 'id', 'transport_from');
    }

    public function cityTo(){
        return $this->hasOne(City::class, 'id', 'transport_to');
    }
}
