<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SitemapController extends Controller
{
    public function index()
    {
        // Configurar las páginas de su sitio web con un resumen en inglés
        $pages = [
            [
                'loc' => url('/'),
                'lastmod' => date('Y-m-d'),
                'priority' => '1.0',
                'freq' => 'daily'
            ],
            [
                'loc' => url('/services'),
                'lastmod' => date('Y-m-d'),
                'priority' => '0.8',
                'freq' => 'weekly'
            ],
            [
                'loc' => url('/quote'),
                'lastmod' => date('Y-m-d'),
                'priority' => '0.8',
                'freq' => 'weekly'
            ]
        ];

        // Generar el contenido del sitemap
        $output = view('sitemap', ['pages' => $pages])->render();

        // Generar la respuesta con el contenido del sitemap
        return response($output)->header('Content-Type', 'text/xml');
    }
}
