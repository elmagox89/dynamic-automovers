<?php

namespace App\Http\Livewire;

use App\Models\State;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;

class StateIndex extends Component
{
    use WithPagination;

    public function calculateReadingTime($content)
    {
        $averageReadingSpeed = 200;
        $wordCount = str_word_count(strip_tags($content));
        $readingTime = ceil($wordCount / $averageReadingSpeed);

        return $readingTime;
    }

    public function render()
    {
        $states = State::latest()->whereStatus(1)->paginate(20);

        foreach ($states as $blog) {
            $blog->humanize_time = Carbon::parse($blog->created_at)->diffForHumans();
            $blog->reading_time = $this->calculateReadingTime($blog->content);
        }

        return view('livewire.state-index', compact('states'))->layout('layouts.main');
    }
}
