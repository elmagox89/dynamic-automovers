<?php

namespace App\Http\Livewire;

use App\Models\City;
use Livewire\Component;

class Autocomplete extends Component
{
    public $selectedCityId;
    public $selectedCityName = '';
    public $searchTerm = '';
    public $showDropdown = false;
    public $highlightedOption = 0;

    public $cities = [];

    public $fieldId;

    public $label;

    protected $listeners = [
        "resetAutocomplete" => "clearSelection"
    ];

    public function mount($field)
    {
        $this->fieldId = $field;
    }

    public function search($value)
    {
        $this->searchTerm = $value;
        $this->showDropdown = true;
        $this->highlightedOption = 0;
        $this->cities = City::selectRaw('id, city, state_code, zip')
            ->where('city', 'like', "{$this->searchTerm}%")
            ->orWhere('zip', 'like', "%{$this->searchTerm}%")
            ->take(5)
            ->get();
    }

    public function selectOption($id, $name)
    {
        $this->showDropdown = false;
        $this->{$this->fieldId} = $id;
        $this->selectedCityName = $name;
        $this->emit("{$this->fieldId}", $id);
    }

    public function clearSelection()
    {
        $this->showDropdown = false;
        $this->{$this->fieldId} = null;
        $this->selectedCityName = '';
    }

    public function openDropdown()
    {
        $this->showDropdown = true;
    }

    public function closeDropdown()
    {
        $this->showDropdown = false;
    }

    public function render()
    {
        return view('livewire.autocomplete', [
            'fieldId' => $this->fieldId
        ]);
    }
}
