<?php

namespace App\Http\Livewire;

use App\Models\Blog;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;

class BlogIndex extends Component
{
    use WithPagination;

    public function calculateReadingTime($content)
    {
        $averageReadingSpeed = 200; // palabras por minuto
        $wordCount = str_word_count(strip_tags($content));
        $readingTime = ceil($wordCount / $averageReadingSpeed);

        return $readingTime;
    }

    public function render()
    {
        $blogs = Blog::latest()->paginate(10);

        foreach ($blogs as $blog) {
            $blog->humanize_time = Carbon::parse($blog->created_at)->diffForHumans();
            $blog->reading_time = $this->calculateReadingTime($blog->content);
        }

        return view('livewire.blog-index', compact('blogs'))->layout('layouts.main');
    }
}
