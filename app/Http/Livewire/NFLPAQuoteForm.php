<?php

namespace App\Http\Livewire;

use App\Mail\NFPLAInformation;
use App\Models\VehicleShipping;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class NFLPAQuoteForm extends Component
{
    public $currentStep = 1;
    public $transport_from;
    public $transport_to;
    public $transport_type;
    public $vehicle_year;
    public $vehicle_make;
    public $vehicle_model;
    public $name;
    public $phone;
    public $email;
    public $is_operable;
    public $pickup_date;

    public $sending = false;

    public $current_former;

    public $raider_id;

    public $type_player;

    public $desired_date;

    protected $messages = [
        'transport_from.required' => 'Please enter the location you will be transporting from.',
        'transport_to.required' => 'Please enter the destination you will be transporting to.',
        'transport_type.required' => 'Please select the type of transport you require.',
        'vehicle_year.required' => 'Please enter the year of your vehicle.',
        'vehicle_make.required' => 'Please enter the make of your vehicle.',
        'vehicle_model.required' => 'Please enter the model of your vehicle.',
        'name.required' => 'Please enter your name.',
        'phone.required' => 'Please enter your phone number.',
        'email.required' => 'Please enter your email address.',
        'email.email' => 'Please enter a valid email address.',
        'is_operable.required' => 'Please let us know if your vehicle is in working condition.',
        'pickup_date.required' => 'Please enter the date you would like your vehicle to be picked up.',
        'pickup_date.date' => 'Please enter a valid date for pickup.',
        'vehicle_year.gt' => 'The vehicle year must be between 1940 and the current year.',
        'vehicle_year.lte' => 'The vehicle year must be between 1940 and the current year.',
    ];

    protected $listeners = [
        "transport_from" => "setValueToTransportFrom",
        "transport_to" => "setValueToTransportTo"
    ];

    public function setValueToTransportFrom($value){
        $this->transport_from = $value;
    }

    public function setValueToTransportTo($value){
        $this->transport_to = $value;
    }

    public function render()
    {
        return view('livewire.nflpa-quote-form', [
            'currentStep' => $this->currentStep
        ]);
    }

    public function nextStep()
    {
        $this->validate();
        $this->currentStep++;
    }

    public function prevStep()
    {
        $this->currentStep--;
    }

    public function submit()
    {
        $this->validate();
        $this->currentStep++;
        $this->sending = true;
        $vehicleShipping = VehicleShipping::create([
            'transport_from' => $this->transport_from,
            'transport_to' => $this->transport_to,
            'transport_type' => $this->transport_type,
            'vehicle_year' => $this->vehicle_year,
            'vehicle_make' => $this->vehicle_make,
            'vehicle_model' => $this->vehicle_model,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'is_operable' => $this->is_operable,
            'current_former' => $this->current_former,
            'raider_id' => $this->raider_id,
            'type_player' => $this->type_player,
            'pickup_date' => $this->pickup_date,
            'desired_date' => $this->desired_date,
        ]);

        $message = "Thank you for your interest in transporting with us. Keep in mind all NFLPA members receive discounts, we will be sending you an email shortly with a discounted quote.";
        session()->flash('message', $message);
        $this->emit('resetAutocomplete');
        $this->sending = false;
        $this->reset();
        $this->dispatchBrowserEvent('fadeOutMessage');

        $emails = ['janiovasquez91@gmail.com', 'daniel@dynamicautomovers.com', 'anthony.harris@nflpa.com', 'support@dynamicautomovers.com', 'elmangones@hotmail.com'];
        Mail::to($this->email)->bcc($emails)->queue(new NFPLAInformation($vehicleShipping));

    }

    public function rules(){
        $currentYear = date('Y');
        $nextYear = $currentYear + 1;

        switch ($this->currentStep) {
            case 1:
                return [
                    'name' => 'required',
                    'phone' => 'required',
                    'email' => 'required|email',
                    'current_former' => 'required',
                    'raider_id' => 'nullable',
                    'type_player' => 'required',
                ];
            case 2:
                return [
                    'transport_from' => 'required',
                    'transport_to' => 'required',
                    'transport_type' => 'required',
                ];
            case 3:
                return [
                    'vehicle_year' => [
                        'required',
                        'numeric',
                        'gt:1939',
                        'lte:' . $nextYear,
                    ],
                    'vehicle_make' => 'required',
                    'vehicle_model' => 'required',
                    'is_operable' => 'required',
                    'pickup_date' => 'required|date',
                    'desired_date' => 'required|date|after_or_equal:pickup_date'
                ];
        }
    }

}
