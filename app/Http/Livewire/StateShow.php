<?php

namespace App\Http\Livewire;

use App\Models\State;
use Illuminate\Support\Facades\View;
use Livewire\Component;

class StateShow extends Component
{
    public $slug;

    protected $listeners = ['updateBlog' => '$refresh'];

    public function mount($slug)
    {
        $this->state = State::where('slug', $slug)->firstOrFail();
        View::share([
            'meta_title' => $this->state->meta_title ?? $this->state->title,
            'meta_description' => $this->state->meta_description,
            'meta_keywords' => $this->state->meta_keywords,
        ]);
        $this->slug = $slug;
    }

    public function render()
    {
        $blog = State::where('slug', $this->slug)->first();
        return view('livewire.state-show', compact('blog'))->layout('layouts.main');
    }
}
