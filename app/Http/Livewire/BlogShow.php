<?php

namespace App\Http\Livewire;

use App\Models\Blog;
use Illuminate\Support\Facades\View;
use Livewire\Component;

class BlogShow extends Component
{
    public $slug;

    protected $listeners = ['updateBlog' => '$refresh'];

    public function mount($slug)
    {
        $this->blog = Blog::where('slug', $slug)->firstOrFail();
        View::share([
            'meta_title' => $this->blog->meta_title ?? $this->blog->title,
            'meta_description' => $this->blog->meta_description,
            'meta_keywords' => $this->blog->meta_keywords,
        ]);
        $this->slug = $slug;
    }

    public function render()
    {
        $blog = Blog::where('slug', $this->slug)->first();
        return view('livewire.blog-show', compact('blog'))->layout('layouts.main');
    }


}
