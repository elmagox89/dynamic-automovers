<?php

namespace App\Http\Livewire;

use InstagramScraper\Exception\InstagramAuthException;
use InstagramScraper\Instagram;
use Livewire\Component;
use GuzzleHttp\Client;
use Phpfastcache\Helper\Psr16Adapter;
use function PHPUnit\Framework\throwException;

class InstagramFeedComponent extends Component
{
    public function render()
    {
        return view('livewire.instagram-feed-component');
    }
}
