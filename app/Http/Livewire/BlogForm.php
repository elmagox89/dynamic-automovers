<?php

namespace App\Http\Livewire;

use App\Models\Blog;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Livewire\Component;

class BlogForm extends Component
{
    use WithFileUploads;

    public $title;
    public $content;
    public $image;
    public $meta_title;
    public $meta_description;
    public $meta_keywords;

    public function render()
    {
        return view('livewire.blog-form')->layout('layouts.main');
    }

    public function saveBlog()
    {
        $this->validate([
            'title' => 'required|string|max:255',
            'content' => 'required|string',
            'image' => 'nullable|image|max:2048', // Asegúrate de validar la imagen
            'meta_title' => 'nullable|string|max:255',
            'meta_description' => 'nullable|string',
            'meta_keywords' => 'nullable|string',
        ]);



        $slug = Str::slug($this->title);
        $slugExists = Blog::where('slug', $slug)->exists();
        $suffix = 1;
        while ($slugExists) {
            $slug = Str::slug($this->title) . '-' . $suffix;
            $slugExists = Blog::where('slug', $slug)->exists();
            $suffix++;
        }

        $meta_title = $this->meta_title ?: $this->title;

        $blog = new Blog([
            'title' => $this->title,
            'content' => $this->content,
            'slug' => $slug,
            'meta_title' => $meta_title,
            'meta_description' => $this->meta_description,
            'meta_keywords' => $this->meta_keywords,
        ]);

        if ($this->image) {
            $imagePath = $this->image->store('blog_images', 'public');
            $blog->image = $imagePath;
        }

        $blog->save();

        session()->flash('message', 'Blog saved successfully.');
    }

}
