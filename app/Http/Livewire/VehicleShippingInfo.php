<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class VehicleShippingInfo extends Component
{
    use WithPagination;

    public $expanded = [];

    const PAGINATION_NUMBER = 10;

    public function mount()
    {

    }

    public function render()
    {
        $currrentUser = Auth::user();
        $shipments = DB::table('vehicle_shipping as vs')
            ->join('cities as c_from', 'c_from.id', '=', 'vs.transport_from')
            ->join('cities as c_to', 'c_to.id', '=', 'vs.transport_to')
            ->select([
                'vs.id',
                'c_from.county as transport_from_county',
                'c_from.state_code as transport_from_state_code',
                'c_from.zip as transport_from_zip',
                'c_to.county as transport_to_county',
                'c_to.state_code as transport_to_state_code',
                'c_to.zip as transport_to_zip',
                'vs.transport_type',
                'vs.name',
                'vs.current_former',
                'vs.raider_id',
                'vs.type_player',
                'vs.vehicle_make',
                'vs.vehicle_model',
                'vs.phone',
                'vs.email',
                'vs.vehicle_year',
                'vs.is_operable',
                'vs.pickup_date',
                'vs.desired_date',
                'vs.created_at'
            ])
            ->when($currrentUser->role == 'nflpa', function ($q){
                $q->whereNotNull('vs.current_former');
            })
            ->orderBy('vs.id', 'desc')
            ->paginate(10);
        return view('livewire.vehicle-shipping-info', compact('shipments'));
    }

    public function toggleExpand($shipmentId)
    {
        $this->expanded[$shipmentId] = !($this->expanded[$shipmentId] ?? false);
    }
}
