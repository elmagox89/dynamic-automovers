<?php

namespace App\Mail;

use App\Models\VehicleShipping;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class TransportInformation extends Mailable
{
    use Queueable, SerializesModels;


    public $transport_from;
    public $transport_to;
    public $transport_type;
    public $vehicle_year;
    public $vehicle_make;
    public $vehicle_model;
    public $name;
    public $phone;
    public $email;
    public $is_operable;
    public $pickup_date;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(VehicleShipping $vehicleShipping)
    {
        ['city' => $cityFrom, 'state_code' => $stateCodeFrom, 'zip' => $zipFrom] = $vehicleShipping->cityFrom()->getResults();
        ['city' => $cityTo, 'state_code' => $stateCodeTo, 'zip' => $zipTo] = $vehicleShipping->cityTo()->getResults();

        $this->transport_from = "$cityFrom, $stateCodeFrom $zipFrom";
        $this->transport_to = "$cityTo, $stateCodeTo $zipTo";
        $this->transport_type = $vehicleShipping->transport_type;
        $this->vehicle_year = $vehicleShipping->vehicle_year;
        $this->vehicle_make = $vehicleShipping->vehicle_make;
        $this->vehicle_model = $vehicleShipping->vehicle_model;
        $this->current_former = $vehicleShipping->current_former ?? null;
        $this->name = $vehicleShipping->name;
        $this->phone = $vehicleShipping->phone;
        $this->email = $vehicleShipping->email;
        $this->is_operable = $vehicleShipping->is_operable;
        $this->pickup_date = $vehicleShipping->pickup_date;
        $this->raider_id = $vehicleShipping->raider_id;
        $this->type_player = $vehicleShipping->type_player;
        $this->pickup_date = $vehicleShipping->pickup_date;
        $this->desired_date = $vehicleShipping->desired_date;
    }

    /**
     * Get the message envelope.
     *
     * @return \Illuminate\Mail\Mailables\Envelope
     */
    public function envelope()
    {
        return new Envelope(
            subject: 'Quote from Dynamic Auto Movers',
        );
    }

    /**
     * Get the message content definition.
     *
     * @return \Illuminate\Mail\Mailables\Content
     */
    public function content()
    {
        return new Content(
            view: 'mail.transport-information',
            with: [
                'transport_from' => $this->transport_from,
                'transport_to' => $this->transport_to,
                'transport_type' => $this->transport_type,
                'vehicle_year' => $this->vehicle_year,
                'vehicle_make' => $this->vehicle_make,
                'vehicle_model' => $this->vehicle_model,
                'name' => $this->name,
                'phone' => $this->phone,
                'email' => $this->email,
                'current_former' => $this->current_former ?? null,
                'raider_id' => $this->raider_id ?? null,
                'type_player' => $this->type_player ?? null,
                'is_operable' => $this->is_operable,
                'pickup_date' => $this->pickup_date,
                'desired_date' => $this->desired_date ?? null
            ]
        );
    }
}
