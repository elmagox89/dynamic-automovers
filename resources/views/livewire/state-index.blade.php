<div class="container mx-auto p-4">
    <div class="flex flex-col p-6">
        @if($states->count())
            <div class="flex flex-wrap -mx-2">
                @foreach($states as $state)
                    <div class="w-full md:w-1/3 px-2 mb-4 flex items-center">
                        <a href="{{ route('state.show', $state->slug) }}" class="ml-4">
                            <img class="rounded w-10 h-10 object-cover" src="{{ asset("storage/" . $state->image) ?: asset('dam.jpg') }}" alt="{{ $state->title }}" />
                        </a>
                        <a href="{{ route('state.show', $state->slug) }}">
                            <h3 class="ml-2 font-bold">{{$state->state}} Car Transport</h3>
                        </a>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
</div>
