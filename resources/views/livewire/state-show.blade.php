@php
    $content_parts = preg_split('/(<p>.*?<\/p>)/', $state->content, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
    $midpoint_index = floor(count($content_parts) / 2);
    array_splice($content_parts, $midpoint_index, 0, ['<!-- insert_component_here -->']);
@endphp

<div class="container mx-auto p-4 pb-10">
    <div class="text-gray-900">
        <h1 class="text-1.5xl sm:text-3xl ">{{ $state->title }}</h1>
    </div>
    <div class="blog">
        @foreach($content_parts as $part)
            @if($part === '<!-- insert_component_here -->')
                @include('livewire.quote-card')
            @else
                {!! $part !!}
            @endif
        @endforeach
    </div>
</div>
