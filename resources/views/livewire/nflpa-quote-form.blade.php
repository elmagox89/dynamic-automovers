<div class="w-full min:h-1/2 max-w-xl bg-white p-10 mx-4 shadow-md rounded-lg ">
    <div class="p-4">
        <img src="{{asset('uploads/2022/03/nflpa-dya-1.png')}}" alt="NFLPA-DAM">
    </div>
    @if (session()->has('message'))
        <div id="flash-message"
             class="mb-4 bg-green-100 text-green-800 border border-green-300 px-4 py-3 rounded flex items-center"
             role="alert">
            <div class="mr-4">
                <svg class="fill-current h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <path d="M0 11l2-2 5 5L18 3l2 2L7 18z"/>
                </svg>
            </div>
            <div>
                <p class="font-bold">Thank you for choosing us! {{$name}}</p>
                <p class="text-sm">{{ session()->get('message') }}</p>
            </div>
        </div>
    @endif
    <div class="step-1" @if($currentStep !== 1) style="display: none" @endif>
        <div class="relative z-0 w-full mb-6 group">
            <input placeholder=" "
                   class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                   type="text" id="name" name="name" wire:model="name">
            <label
                class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                for="name">NFLPA Member Name:</label>
            @error('name') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="relative z-0 w-full mb-6 group">
            <label for="current_former" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Current or former NFL Team:</label>
            <select id="current_former" name="current_former" wire:model="current_former" class="block bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                <option value="">---</option>
                <option value="Arizona Cardinals">Arizona Cardinals</option>
                <option value="Atlanta Falcons">Atlanta Falcons</option>
                <option value="Baltimore Ravens">Baltimore Ravens</option>
                <option value="Buffalo Bills">Buffalo Bills</option>
                <option value="Carolina Panthers">Carolina Panthers</option>
                <option value="Chicago Bears">Chicago Bears</option>
                <option value="Cincinnati Bengals">Cincinnati Bengals</option>
                <option value="Cleveland Browns">Cleveland Browns</option>
                <option value="Dallas Cowboys">Dallas Cowboys</option>
                <option value="Denver Broncos">Denver Broncos</option>
                <option value="Detroit Lions">Detroit Lions</option>
                <option value="Green Bay Packers">Green Bay Packers</option>
                <option value="Houston Texans">Houston Texans</option>
                <option value="Indianapolis Colts">Indianapolis Colts</option>
                <option value="Jacksonville Jaguars">Jacksonville Jaguars</option>
                <option value="Kansas City Chiefs">Kansas City Chiefs</option>
                <option value="Las Vegas Raiders">Las Vegas Raiders</option>
                <option value="Los Angeles Chargers">Los Angeles Chargers</option>
                <option value="Los Angeles Rams">Los Angeles Rams</option>
                <option value="Miami Dolphins">Miami Dolphins</option>
                <option value="Minnesota Vikings">Minnesota Vikings</option>
                <option value="New England Patriots">New England Patriots</option>
                <option value="New Orleans Saints">New Orleans Saints</option>
                <option value="New York Giants">New York Giants</option>
                <option value="New York Jets">New York Jets</option>
                <option value="Philadelphia Eagles">Philadelphia Eagles</option>
                <option value="Pittsburgh Steelers">Pittsburgh Steelers</option>
                <option value="San Francisco 49ers">San Francisco 49ers</option>
                <option value="Seattle Seahawks">Seattle Seahawks</option>
                <option value="Tampa Bay Buccaneers">Tampa Bay Buccaneers</option>
                <option value="Tennessee Titans">Tennessee Titans</option>
                <option value="Washington Commanders">Washington Commanders</option>
            </select>
            <!-- Si deseas mostrar mensajes de error -->
            @error('current_former') <span class="error">{{ $message }}</span> @enderror
        </div>

        <div class="relative z-0 w-full mb-6 group">
            <input placeholder=" "
                   class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                   type="raider_id" id="raider_id" name="raider_id" wire:model="raider_id">
            <label
                class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                for="raider_id">Raider ID #:</label>
            @error('raider_id') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label class="block text-gray-900 text-sm font-bold mb-2">Type of player *:</label>
            <input type="radio" name="type_player" value="active" wire:model="type_player" class="mr-2 text-gray-400"> Active Player
            <input type="radio" name="type_player" value="former" wire:model="type_player" class="mr-2 text-gray-400"> Former Player
            <input type="radio" name="type_player" value="other" wire:model="type_player" class="mx-2 text-gray-400"> Other
            <div>
                @error('type_player') <span class="error">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="relative z-0 w-full mb-6 group">
            <input placeholder=" "
                   class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                   type="tel" id="phone" name="phone" wire:model="phone">
            <label
                class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                for="phone">Phone:</label>
            @error('phone') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="relative z-0 w-full mb-6 group">
            <input placeholder=" "
                   class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                   type="email" id="email" name="email" wire:model="email">
            <label
                class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                for="email">Email:</label>
            @error('email') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <button wire:click="nextStep"
                    class="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 w-full transition duration-500 hover:shadow-md">
                Vehicle Details
            </button>
        </div>

    </div>
    <div class="step-2" @if($currentStep !== 2) style="display: none" @endif>
        <h4 class="text-2xl my-6">
            Get A <span class="font-medium text-red-600">Free</span> <span class="font-normal">Instant Quote</span> Or
            <span class="font-medium text-red-600">Call</span> <span class="font-normal">Now</span> <span
                class="font-medium text-red-600">(888) 210 9906</span>
        </h4>
        <div class="mb-4">
            <livewire:autocomplete field="transport_from" label="Transport From"/>
            @error('transport_from') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <livewire:autocomplete field="transport_to" label="Transport To"/>
            @error('transport_to') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label class="block text-gray-900 text-sm font-bold mb-2">Transport Type:</label>
            <input type="radio" name="transport_type" value="open" wire:model="transport_type" class="mr-2"> Open
            <input type="radio" name="transport_type" value="enclosed" wire:model="transport_type" class="mx-2">
            Enclosed
            <div>
                @error('transport_type') <span class="error">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="mb-4">
            <button wire:click="nextStep"
                    class="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 w-full transition duration-500 hover:shadow-md">
                Next
            </button>
        </div>
    </div>
    <div class="step-3" @if($currentStep !== 3) style="display: none" @endif>
        <div class="relative z-0 w-full mb-6 group">
            <input type="number" id="vehicle_year" name="vehicle_year" wire:model="vehicle_year" autocomplete="off"
                   class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                   placeholder=" " required/>
            <label for="vehicle_year"
                   class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Vehicle
                Year:</label>
            @error('vehicle_year') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="relative z-0 w-full mb-6 group">
            <input type="text" id="vehicle_make" name="vehicle_make" wire:model="vehicle_make" autocomplete="off"
                   class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                   placeholder=" " required/>
            <label for="vehicle_make"
                   class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Vehicle
                Make:</label>
            @error('vehicle_make') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="relative z-0 w-full mb-6 group">
            <input type="text" id="vehicle_model" name="vehicle_model" wire:model="vehicle_model"
                   class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                   placeholder=" " required/>
            <label for="vehicle_model"
                   class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Vehicle
                Model:</label>
            @error('vehicle_model') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label class="block text-gray-900 text-sm font-bold mb-2" for="is_operable">Is the vehicle operable?</label>
            <div class="flex">
                <input type="radio" id="yes" name="is_operable" value="yes" wire:model="is_operable">
                <label for="yes" class="ml-2">Yes</label>
                <input type="radio" id="no" name="is_operable" value="no" wire:model="is_operable" class="ml-4">
                <label for="no" class="ml-2">No</label>
            </div>
            @error('is_operable') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label class="block text-gray-900 text-sm font-bold mb-2" for="pickup_date">Desired Pickup Date:</label>
            <input
                class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker hover:shadow-md transition duration-500"
                type="date" id="pickup_date" name="pickup_date" wire:model="pickup_date">
            @error('pickup_date') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label class="block text-gray-900 text-sm font-bold mb-2" for="pickup_date">Desired Delivery Date:</label>
            <input
                class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker hover:shadow-md transition duration-500"
                type="date" id="desired_date" name="desired_date" wire:model="desired_date">
            @error('desired_date') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4 flex justify-between gap-1" wire:loading.remove wire:target="submit">
            <button wire:click="prevStep"
                    class="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 w-1/2 hover:shadow-md transition duration-500">
                Back
            </button>
            <button wire:click="submit" wire:loading.attr="disabled"
                    class="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 w-1/2 hover:shadow-md transition duration-500">
                Submit
            </button>
        </div>
        <div wire:loading wire:target="submit" class="text-red-500 font-bold mt-2 w-full text-center">Sending Quote...
        </div>
    </div>
    <div class="text-gray-400">
        <p>For any questions or concerns please feel free to e-mail <b>anthony.harris@nflpa.com</b> or <b>daniel@dynamicautomovers.com</b></p>
    </div>
</div>

</div>


