<div class="container mx-auto p-4">
    <div class="flex flex-col p-6">
        @if($blogs->count())
            <div class="flex flex-wrap">
                @foreach($blogs as $blog)
                    <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/2 p-4">
                        <div class="max-w-6xl min-h-full bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                            <a href="{{ route('blog.show', $blog->slug) }}">
                                <img class="rounded-t-lg w-full h-60 object-cover" src="{{ asset("storage/" . $blog->image) ?: asset('dam.jpg') }}" alt="{{ $blog->title }}" />
                            </a>
                            <div class="p-5">
                                <a href="{{ route('blog.show', $blog->slug) }}">
                                    <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{{ $blog->title }}</h5>
                                </a>
                                <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">{{ $blog->meta_description }}</p>
                                <p class="text-gray-600 text-sm">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline mr-1 text-gray-600" viewBox="0 0 20 20" fill="currentColor">
                                        <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v1h14V3a1 1 0 0 0-1-1H2zm13 3H1v9a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V5z"/>
                                        <path d="M11 7.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z"/>
                                    </svg>{{ $blog->humanize_time }} |
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 inline mr-1 text-gray-600" viewBox="0 0 20 20" fill="currentColor">
                                        <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"/>
                                        <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"/>
                                    </svg>{{ $blog->reading_time }} min
                                </p>
                                <a href="{{ route('blog.show', $blog->slug) }}" class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                    Read more
                                    <svg aria-hidden="true" class="w-4 h-4 ml-2 -mr-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="mt-4">
                {{ $blogs->links() }}
            </div>
        @else
            <div
                class="bg-blue-100 border border-blue-400 text-blue-700 px-4 py-3 rounded relative w-full sm:w-3/4 md:w-1/2 lg:w-1/3 mx-auto mt-10"
                role="alert">
                <strong class="font-bold">No Blogs Found!</strong>
                <span class="block sm:inline">Currently, there are no blogs available to display.</span>
            </div>
        @endif
    </div>
</div>
