<div class="flex flex-col p-5">
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead class="bg-gray-50">
                    <tr>
                        <th class="px-3 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">ID</th>
                        <th class="px-3 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name</th>
                        <th class="px-3 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider hidden md:table-cell">Transport From</th>
                        <th class="px-3 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider hidden md:table-cell">Transport To</th>
                        <th class="px-3 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider hidden md:table-cell">Quote Date</th>
                        <th class="px-3 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Origin</th>
                        <th class="px-3 py-2 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Details</th>
                    </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                    @foreach ($shipments as $shipment)
                        <tr>
                            <td class="px-3 py-2 whitespace-nowrap">{{ $shipment->id }}</td>
                            <td class="px-3 py-2 whitespace-nowrap">{{ $shipment->name }}</td>
                            <td class="px-3 py-2 whitespace-nowrap hidden md:table-cell">{{ $shipment->transport_from_county }}, {{ $shipment->transport_from_state_code }}, {{ $shipment->transport_from_zip }}</td>
                            <td class="px-3 py-2 whitespace-nowrap hidden md:table-cell">{{ $shipment->transport_to_county }}, {{ $shipment->transport_to_state_code }}, {{ $shipment->transport_to_zip }}</td>
                            <td class="px-3 py-2 whitespace-nowrap hidden md:table-cell">{{ $shipment->created_at }}</td>
                            <td class="px-3 py-2 whitespace-nowrap">
                                <span class="{{ $shipment->current_former ? 'bg-red-500' : 'bg-gray-500' }} text-white px-2 py-0.5 rounded">
                                    {{ $shipment->current_former ? 'NFLPA' : 'DAM' }}
                                </span>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <button
                                    wire:click="toggleExpand({{ $shipment->id }})"
                                    class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-3 rounded shadow transition duration-300 ease-in-out focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                                >
                                    Show Details
                                </button>
                            </td>
                        </tr>
                        @if($expanded[$shipment->id] ?? false)
                            <tr>
                                <td colspan="100%">
                                    <div class="grid grid-cols-1 md:grid-cols-2 gap-4 p-4 bg-gray-50 border border-gray-200 rounded-md">
                                        <div><strong class="text-gray-700">Transport From:</strong> <span class="text-gray-500">{{ $shipment->transport_from_county }}, {{ $shipment->transport_from_state_code }}, {{ $shipment->transport_from_zip }}</span></div>
                                        <div><strong class="text-gray-700">Transport To:</strong> <span class="text-gray-500">{{ $shipment->transport_to_county }}, {{ $shipment->transport_to_state_code }}, {{ $shipment->transport_to_zip }}</span></div>
                                        <div><strong class="text-gray-700">Transport Type:</strong> <span class="text-gray-500">{{ $shipment->transport_type }}</span></div>
                                        <div><strong class="text-gray-700">Name:</strong> <span class="text-gray-500">{{ $shipment->name }}</span></div>
                                        <div><strong class="text-gray-700">Current Former:</strong> <span class="text-gray-500">{{ $shipment->current_former }}</span></div>
                                        <div><strong class="text-gray-700">Raider ID:</strong> <span class="text-gray-500">{{ $shipment->raider_id }}</span></div>
                                        <div><strong class="text-gray-700">Type Player:</strong> <span class="text-gray-500">{{ $shipment->type_player }}</span></div>
                                        <div><strong class="text-gray-700">Vehicle Make:</strong> <span class="text-gray-500">{{ $shipment->vehicle_make }}</span></div>
                                        <div><strong class="text-gray-700">Vehicle Model:</strong> <span class="text-gray-500">{{ $shipment->vehicle_model }}</span></div>
                                        <div>
                                            <strong class="text-gray-700">Phone:</strong>
                                            <a class="text-white px-2 py-1 rounded border border-green-500" href="tel:{{ $shipment->phone }}" class="text-blue-500 hover:underline">
                                                <span class="text-gray-500">{{ $shipment->phone }}</span>
                                            </a>
                                        </div>
                                        <div><strong class="text-gray-700">Email:</strong> <span class="text-gray-500">{{ $shipment->email }}</span></div>
                                        <div><strong class="text-gray-700">Vehicle Year:</strong> <span class="text-gray-500">{{ $shipment->vehicle_year }}</span></div>
                                        <div><strong class="text-gray-700">Is Operable:</strong> <span class="text-gray-500">{{ $shipment->is_operable }}</span></div>
                                        <div><strong class="text-gray-700">Pickup Date:</strong> <span class="text-gray-500">{{ $shipment->pickup_date }}</span></div>
                                        <div><strong class="text-gray-700">Desired Date:</strong> <span class="text-gray-500">{{ $shipment->desired_date }}</span></div>
                                        <div><strong class="text-gray-700">Created At:</strong> <span class="text-gray-500">{{ $shipment->created_at }}</span></div>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Paginación -->
    <div class="mt-4">
        {{ $shipments->links() }}
    </div>
</div>
