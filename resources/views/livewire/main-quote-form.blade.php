<div class="w-full min:h-1/2 max-w-xl bg-white p-10 mx-4 shadow-md rounded-lg ">
    @if (session()->has('message'))
        <div id="flash-message"
             class="mb-4 bg-green-100 text-green-800 border border-green-300 px-4 py-3 rounded flex items-center"
             role="alert">
            <div class="mr-4">
                <svg class="fill-current h-6 w-6 text-green-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <path d="M0 11l2-2 5 5L18 3l2 2L7 18z"/>
                </svg>
            </div>
            <div>
                <p class="font-bold">Thank you for choosing us! {{$name}}</p>
                <p class="text-sm">{{ session()->get('message') }}</p>
            </div>
        </div>
    @endif
    <div class="step-1" @if($currentStep !== 1) style="display: none" @endif>
        <div class="flex">
            <h4 class="text-2xl my-6 w-75 pr-3">
                Get A <span class="font-bold text-red-600">Free</span> <span class="font-bold">Instant Quote</span> Or <span
                    class="font-bold">Call Now</span> <br><span class="font-medium text-red-600">(888) 210 9906</span>
            </h4>
            <div class="w-25">
                <a href="https://www.bbb.org/us/fl/miami/profile/relocation-services/dynamic-auto-movers-llc-0633-90125570" target="_blank"><img src="{{ asset('/uploads/2022/03/bbb-1.jpg') }}" class="my-auto w-16 mx-auto my-6 opacity-80 hover:opacity-100 transition-opacity" alt="BBB"></a>
            </div>
        </div>
        <div class="mb-4">
            <livewire:autocomplete field="transport_from" label="Transport From"/>
            @error('transport_from') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <livewire:autocomplete field="transport_to" label="Transport To"/>
            @error('transport_to') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label class="block text-gray-900 text-sm font-bold mb-2" for="transport_type">Transport Type:</label>
            <input type="radio" name="transport_type" value="open" wire:model="transport_type" class="mr-2"> Open
            <input type="radio" name="transport_type" value="enclosed" wire:model="transport_type" class="mx-2">
            Enclosed
            <div>
                @error('transport_type') <span class="error">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="mb-4">
            <button wire:click="nextStep"
                    class="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 w-full transition duration-500 hover:shadow-md">
                Next
            </button>
        </div>
    </div>
    <div class="step-2" @if($currentStep !== 2) style="display: none" @endif>
        <div class="relative z-0 w-full mb-6 group">
            <input type="number" id="vehicle_year" name="vehicle_year" wire:model="vehicle_year" autocomplete="off"
                   class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                   placeholder=" " required/>
            <label for="vehicle_year"
                   class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Vehicle
                Year:</label>
            @error('vehicle_year') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="relative z-0 w-full mb-6 group">
            <input type="text" id="vehicle_make" name="vehicle_make" wire:model="vehicle_make" autocomplete="off"
                   class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                   placeholder=" " required/>
            <label for="vehicle_make"
                   class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Vehicle
                Make:</label>
            @error('vehicle_make') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="relative z-0 w-full mb-6 group">
            <input type="text" id="vehicle_model" name="vehicle_model" wire:model="vehicle_model" autocomplete="off"
                   class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                   placeholder=" " required/>
            <label for="vehicle_model"
                   class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Vehicle
                Model:</label>
            @error('vehicle_model') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4 flex justify-between gap-1">
            <button wire:click="prevStep"
                    class="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 w-1/2 hover:shadow-md transition duration-500">
                Previous
            </button>
            <button wire:click="nextStep"
                    class="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 w-1/2 hover:shadow-md transition duration-500">
                Next
            </button>
        </div>
    </div>
    <div class="step-3" @if($currentStep !== 3) style="display: none" @endif>
        <div class="relative z-0 w-full mb-6 group">
            <input placeholder=" " autocomplete="off"
                   class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                   type="text" id="name" name="name" wire:model="name">
            <label
                class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                for="name">Name:</label>
            @error('name') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="relative z-0 w-full mb-6 group">
            <input placeholder=" " autocomplete="off"
                   class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                   type="tel" id="phone" name="phone" wire:model="phone">
            <label
                class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                for="phone">Phone:</label>
            @error('phone') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="relative z-0 w-full mb-6 group">
            <input placeholder=" " autocomplete="off"
                   class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                   type="email" id="email" name="email" wire:model="email">
            <label
                class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
                for="email">Email:</label>
            @error('email') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label class="block text-gray-900 text-sm font-bold mb-2" for="is_operable">Is the vehicle operable?</label>
            <div class="flex">
                <input type="radio" id="yes" name="is_operable" value="yes" wire:model="is_operable">
                <label for="yes" class="ml-2">Yes</label>
                <input type="radio" id="no" name="is_operable" value="no" wire:model="is_operable" class="ml-4">
                <label for="no" class="ml-2">No</label>
            </div>
            @error('is_operable') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label class="block text-gray-900 text-sm font-bold mb-2" for="pickup_date">Pickup date:</label>
            <input
                class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker hover:shadow-md transition duration-500"
                type="date" id="pickup_date" name="pickup_date" wire:model="pickup_date">
            @error('pickup_date') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4 flex justify-between gap-1" wire:loading.remove wire:target="submit">
            <button wire:click="prevStep"
                    class="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 w-1/2 hover:shadow-md transition duration-500">
                Back
            </button>
            <button wire:click="submit" wire:loading.attr="disabled" onclick="gtag_report_conversion()"
                    class="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 w-1/2 hover:shadow-md transition duration-500">
                Submit
            </button>
        </div>
        <div wire:loading wire:target="submit" class="text-red-500 font-bold mt-2 w-full text-center">Sending Quote...</div>
    </div>
</div>
</div>


