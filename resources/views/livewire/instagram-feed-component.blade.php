<div>
    <div id="instafeed"></div>
</div>
@push("scripts")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/instafeed.js/1.4.0/instafeed.min.js"></script>
    <script type="text/javascript">
        var feed = new Instafeed({
            accessToken: 'your-token'
        });
        //feed.run();
    </script>
@endpush
