<div class="container mx-auto my-6">
    <div class="bg-gradient-to-tr from-sky-800 to-blue-900 text-white p-6 rounded-lg shadow-md w-full max-w-6xl">
        <h2 class="text-xl font-bold text-white uppercase">Get a free quote</h2>
        <p class="mb-6 text-white">Request a personalized quote for long-distance autotransport from our professional movers. Get a customized estimate for your unique needs with our easy-to-use quote form.</p>
        <a href="{{ route('quote') }}" class="bg-white text-blue-900 font-semibold px-4 py-2 rounded-lg hover:bg-blue-200 transition-colors">Request a Quote</a>
    </div>
</div>
