<div >
    <input type="hidden" wire:model="{{$fieldId}}">
    <div class="relative z-0 w-full mb-6 group">
        <input type="search" name="{{$fieldId}}" id="{{$fieldId}}" placeholder=" "
               class="block py-2 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
               wire:keydown.escape="clearSelection"
               wire:input="search($event.target.value)"
               value="{{ $selectedCityName }}"
               autocomplete="off"
        >
        <label for="{{$fieldId}}"
               class="peer-focus:font-normal absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-90 peer-focus:-translate-y-6">{{$label}}
        </label>
    </div>
    <div class="dropdown-menu" @if(!$showDropdown) style="display: none" @endif>
        <div class="shadow-md w-full rounded-lg bg-white w-full" wire:click.away="closeDropdown">
            <ul class="absolute z-10 bg-white shadow-md list-reset">
                @foreach ($cities as $city)
                    <li class="px-4 py-2 cursor-pointer hover:bg-gray-200" wire:click="selectOption({{ $city->id }},'{{ $city->city }}, {{ $city->state_code }} {{ $city->zip }}')">
                        {{ $city->city }}, {{ $city->state_code }} {{ $city->zip }}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
