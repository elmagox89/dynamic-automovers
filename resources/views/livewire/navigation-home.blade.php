<nav x-data="{ open: false }" class="bg-gray-900  ">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-20">
            <div class="shrink-0 flex items-center">
                <a href="{{ route('home') }}">
                    <x-jet-application-mark class="h-12 w-auto" alt="Dynamic Auto Movers"/>
                </a>
            </div>
            <div class="flex align-middle">
                <div class="hidden space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-nav-link href="{{ route('home') }}" :active="request()->routeIs('home')">
                        {{ __('Home') }}
                    </x-jet-nav-link>
                </div>
                <div class="hidden space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-nav-link href="{{ route('about') }}" :active="request()->routeIs('about')">
                        {{ __('About') }}
                    </x-jet-nav-link>
                </div>
                <div class="hidden space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-nav-link href="{{ route('services') }}" :active="request()->routeIs('services')">
                        {{ __('Services') }}
                    </x-jet-nav-link>
                </div>
                <div class="hidden space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-nav-link href="{{ route('states.index') }}" :active="request()->routeIs('states.index')">
                        {{ __('Ship Car to Another State') }}
                    </x-jet-nav-link>
                </div>
                <div class="hidden space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-nav-link href="{{ route('process') }}" :active="request()->routeIs('process')">
                        {{ __('Process') }}
                    </x-jet-nav-link>
                </div>
                <div class="hidden space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-nav-link href="{{ route('faqs') }}" :active="request()->routeIs('faqs')">
                        {{ __('FAQs') }}
                    </x-jet-nav-link>
                </div>
                <div class="hidden space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-nav-link href="{{ route('privacy-policy') }}" :active="request()->routeIs('privacy-policy')">
                        {{ __('Privacy Policy') }}
                    </x-jet-nav-link>
                </div>
                <div class="hidden space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-nav-link href="{{ route('blog.index') }}" :active="request()->routeIs('blog')">
                        {{ __('Blog') }}
                    </x-jet-nav-link>
                </div>
                <div class="hidden space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-nav-link href="{{ route('nflpa') }}" :active="request()->routeIs('nflpa')">
                        {{ __('NFLPA') }}
                    </x-jet-nav-link>
                </div>
                <div class="hidden space-x-6 sm:-my-px sm:ml-10 sm:flex items-center">
                    <a class="inline-flex items-center leading-5 bg-red-700 hover:bg-red-600 text-white font-bold py-2 px-4 border-b-4 border-red-900 hover:border-red-700 rounded h-12"
                       href="{{ route('quote') }}">
                        FREE QUOTE
                    </a>
                </div>
            </div>
            <!-- Hamburger -->
            <div class="-mr-2 flex items-center sm:hidden">
                <button @click="open = ! open"
                        class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition" aria-label="menu">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex"
                              stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              d="M4 6h16M4 12h16M4 18h16"/>
                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round"
                              stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
                    </svg>
                </button>
            </div>
        </div>
    </div>
    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">
        <div class="pt-4 pb-1 border-t border-gray-600">
            <div class="mt-3 space-y-1">
                <div class="space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-responsive-nav-link href="{{ route('home') }}" :active="request()->routeIs('home')">
                        {{ __('Home') }}
                    </x-jet-responsive-nav-link>
                </div>
                <div class="space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-responsive-nav-link href="{{ route('about') }}" :active="request()->routeIs('about')">
                        {{ __('About') }}
                    </x-jet-responsive-nav-link>
                </div>
                <div class="space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-responsive-nav-link href="{{ route('services') }}" :active="request()->routeIs('services')">
                        {{ __('Services') }}
                    </x-jet-responsive-nav-link>
                </div>
                <div class="space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-responsive-nav-link href="{{ route('process') }}" :active="request()->routeIs('process')">
                        {{ __('Process') }}
                    </x-jet-responsive-nav-link>
                </div>
                <div class="space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-responsive-nav-link href="{{ route('states.index') }}"
                                               :active="request()->routeIs('states.index')">
                        {{ __('Ship Car to Another State') }}
                    </x-jet-responsive-nav-link>
                </div>
                <div class="space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-responsive-nav-link href="{{ route('faqs') }}" :active="request()->routeIs('faqs')">
                        {{ __('FAQs') }}
                    </x-jet-responsive-nav-link>
                </div>
                <div class="space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-responsive-nav-link href="{{ route('privacy-policy') }}"
                                               :active="request()->routeIs('privacy-policy')">
                        {{ __('Privacy Policy') }}
                    </x-jet-responsive-nav-link>
                </div>
                <div class="space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-responsive-nav-link href="{{ route('blog.index') }}" :active="request()->routeIs('blog')">
                        {{ __('Blog') }}
                    </x-jet-responsive-nav-link>
                </div>
                <div class="space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-responsive-nav-link href="{{ route('nflpa') }}" :active="request()->routeIs('nflpa')">
                        {{ __('NFLPA') }}
                    </x-jet-responsive-nav-link>
                </div>
                <div class="space-x-6 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-responsive-nav-link href="{{ route('quote') }}" :active="request()->routeIs('quote')">
                        {{ __('FREE QUOTE') }}
                    </x-jet-responsive-nav-link>
                </div>
            </div>
        </div>
    </div>
</nav>
