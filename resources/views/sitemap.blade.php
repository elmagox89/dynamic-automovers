<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($pages as $page)
        <url>
            <loc>{{ $page['loc'] }}</loc>
            <lastmod>{{ $page['lastmod'] }}</lastmod>
            <priority>{{ $page['priority'] }}</priority>
            <changefreq>{{ $page['freq'] }}</changefreq>
        </url>
    @endforeach
</urlset>
