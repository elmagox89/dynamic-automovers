@props(['active'])

@php
$classes = ($active ?? false)
            ? 'uppercase font-bold inline-flex items-center px-1 pt-1 border-b-2 border-indigo-400 text-sm leading-5 text-gray-400 focus:outline-none hover:outline-none transition-none'
            : 'uppercase font-bold inline-flex items-center px-1 pt-1 border-b-2 border-transparent text-sm leading-5 text-gray-400 focus:outline-none hover:text-white focus:text-gray-700 transition';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
