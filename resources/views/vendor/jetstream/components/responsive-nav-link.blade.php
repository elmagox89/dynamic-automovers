@props(['active'])

@php
$classes = ($active ?? false)
            ? 'block pl-3 pr-4 py-2 border-l-4 border-red-700 text-base font-medium text-gray-200 bg-gray-700 focus:outline-none focus:text-indigo-800 focus:bg-gray-700 focus:border-red-700 transition'
            : 'block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-100 hover:text-gray-700 hover:bg-gray-600 hover:border-gray-600 focus:outline-none focus:text-gray-700 focus:bg-gray-600 focus:border-red-700 transition';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
