<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="{{ $meta_description ?? 'Call us to transport your vehicle, safe and quick. Auto transports' }}">
        <meta name="author" content="elmagox">
        <meta property="og:type" content="website">
        <meta property="og:title" content="{{ $meta_title ?? 'Dynamic Auto Movers - Safe and Quick Vehicle Transport' }}">
        <meta property="og:site_name" content="Dynamic Auto Movers">
        <meta property="og:url" content="https://dynamicautomovers.com/">
        <meta property="og:description" content="{{ $meta_description ?? 'Call us to transport your vehicle, safe and quick. Auto transports' }}">
        <meta name="keywords" content="{{ $meta_keywords ?? 'auto transports, auto movers, car transport' }}">
        <meta name="robots" content="index, follow">
        <link rel="alternate" hreflang="en" href="https://dynamicautomovers.com/" />
        <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
        <title>{{ $meta_title ?? config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;700&display=swap">

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])

        <!-- Styles -->
        @livewireStyles
        <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.3/datepicker.min.js"></script>
        <!-- Google tag (gtag.js) -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-W4SPQXJWX7"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-W4SPQXJWX7');
        </script>
        <script>
            function gtag_report_conversion() {
                gtag('event', 'conversion', {
                    'send_to': 'AW-975599416/hhO0CNDgwZEYELjumdED'
                });
            }
        </script>
    </head>
    <body class="font-sans antialiased">
        <x-jet-banner />

        <div class="min-h-screen bg-gray-100">
            @livewire('navigation-home')
            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>



        @stack('modals')
        @livewireScripts

        @stack('scripts')


        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-938696902"></script>
        <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-938696902'); </script>
        <div class="bg-gray-900 text-gray-300">
            <div class="max-w-7xl mx-auto px-4 py-8 grid grid-cols-3 gap-4">
                <div>
                    <ul class="space-y-2">
                        <li><a href="{{ route('about') }}" class="hover:text-gray-100 underline">About Us</a></li>
                        <li><a href="{{ route('locations') }}" class="hover:text-gray-100 underline">Locations</a></li>
                        <li><a href="{{ route('faqs') }}" class="hover:text-gray-100 underline">FAQs</a></li>
                    </ul>
                </div>
                <div>
                    <ul class="space-y-2">
                        <li><a href="{{ route('services') }}" class="hover:text-gray-100 underline">Services</a></li>
                        <li><a href="{{ route('nflpa') }}" class="hover:text-gray-100 underline">Quote NFLPA</a></li>
                    </ul>
                </div>
                <div>
                    <ul class="space-y-2">
                        <li><a href="{{ route('quote') }}" class="hover:text-gray-100 underline">Get a Quote</a></li>
                        <li><a href="{{ route('privacy-policy') }}" class="hover:text-gray-100 underline">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <div class="max-w-7xl mx-auto px-4 py-4 flex items-center justify-between">
                <a href="{{ route('dashboard') }}">
                    <x-jet-application-mark class="h-12 w-auto" alt="Dynamic Auto Movers"/>
                </a>
                <div class="text-sm">&copy; 2023 Dynamic Auto Movers. All rights reserved.</div>
            </div>
        </div>
        <script>
            async function fetchInstagramData() {
                try {
                    const response = await fetch('https://feeds.behold.so/Q6yg4CgCjL2iT8Gkgd0L');
                    return await response.json();
                } catch (error) {
                    console.error(error);
                }
            }

            async function renderInstagramData() {
                const dataArray = await fetchInstagramData();
                let cards = '';

                dataArray.forEach(data => {
                    console.log(data)
                    const hashtags = Array.isArray(data.hashtags) ? data.hashtags.slice(0, 5).map(tag => `#${tag}`).join(' ') : '';
                    const caption = data.prunedCaption || '';
                    const mediaUrl = data.mediaUrl || '';
                    const thumbnailUrl = data.thumbnailUrl || '';
                    const timestamp = formatTimestamp(data.timestamp) || '';

                    const card = `
                        <article class="relative isolate flex flex-col justify-end overflow-hidden rounded-2xl bg-gray-900 px-8 pb-8 sm:pt-48 lg:pt-80">
                            <img src="${thumbnailUrl}" alt="${caption}" class="absolute inset-0 -z-10 h-full w-full object-cover object-center">
                            <video class="absolute inset-0 w-full h-full object-cover object-center -z-10 hidden" onended="resetVideo(this)">
                                <source src="${mediaUrl}" type="video/mp4">
                            </video>
                            <div class="video-overlay absolute inset-0 -z-10 bg-gradient-to-t from-gray-900 via-gray-900/40"></div>
                            <div class="absolute inset-0 -z-10 rounded-2xl ring-1 ring-inset ring-gray-900/10">
                                <svg class="play-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="white" width="48" height="48">
                                    <path d="M0 0h24v24H0z" fill="none"/>
                                    <path d="M8 5v14l11-7z"/>
                                </svg>
                                <svg class="pause-icon hidden" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="white" width="48" height="48">
                                    <path d="M0 0h24v24H0z" fill="none"/>
                                    <path d="M9 5h2v14H9V5zm4 0h2v14h-2V5z"/>
                                </svg>
                            </div>
                            <div class="flex flex-wrap items-center gap-y-1 overflow-hidden text-sm leading-6 text-gray-300">
                                <time datetime="${timestamp}" class="mr-8">${timestamp}</time>
                                <div class="-ml-4 flex items-center gap-x-4">
                                    <div class="flex gap-x-2.5">
                                        ${hashtags}
                                    </div>
                                </div>
                            </div>
                            <h3 class="mt-3 text-lg font-semibold leading-6 text-white">
                                <span class="absolute inset-0 play-button" ></span>
                                ${caption}
                            </h3>
                        </article>
                    `;
                    cards += card;
                });

                document.getElementById('instagram').innerHTML = cards;
            }

            function toggleVideoPlayback(playButton) {
                const articleElement = playButton.closest('article');
                const videoElement = articleElement.querySelector('video');
                const playIcon = articleElement.querySelector('.play-icon');
                const pauseIcon = articleElement.querySelector('.pause-icon');

                if (videoElement.paused) {
                    videoElement.play();
                    videoElement.classList.remove('hidden')
                    playIcon.classList.add('hidden');
                    pauseIcon.classList.remove('hidden');
                } else {
                    videoElement.pause();
                    videoElement.classList.add('hidden')
                    playIcon.classList.remove('hidden');
                    pauseIcon.classList.add('hidden');
                }
            }

            function resetVideo(videoElement) {
                const playButton = videoElement.nextElementSibling;
                const playIcon = playButton.querySelector('.play-icon');
                const pauseIcon = playButton.querySelector('.pause-icon');

                videoElement.load();
                playIcon.classList.remove('hidden');
                pauseIcon.classList.add('hidden');
            }

            function formatTimestamp(timestamp) {
                const date = new Date(timestamp);
                const options = {
                    year: 'numeric',
                    month: 'short',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric',
                    second: 'numeric',
                    hour12: true
                };
                return date.toLocaleString('en-US', options);
            }

            document.addEventListener('click', function(event) {
                if (event.target.classList.contains('play-button')) {
                    toggleVideoPlayback(event.target);
                }
            });

            renderInstagramData();
        </script>
    </body>
</html>
