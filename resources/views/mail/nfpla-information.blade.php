<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Transport Information</title>
    <style>
        body {
            background-color: #f4f4f4;
            font-family: sans-serif;
            font-size: 16px;
            line-height: 1.6;
            color: #444444;
            padding: 30px;
            max-width: 600px;
            margin: 0 auto;
        }
        h2 {
            font-size: 24px;
            margin-top: 0;
            margin-bottom: 30px;
            text-align: center;
        }
        p {
            margin-top: 0;
            margin-bottom: 1.2em;
        }
        .table-wrapper {
            overflow-x: auto;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
        }
        th,
        td {
            padding: 10px;
            text-align: left;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }
        th {
            font-weight: bold;
        }
        @media screen and (max-width: 600px) {
            body {
                padding: 15px;
            }
        }
    </style>
</head>
<body>
<h2>Quote Information</h2>
<p>Dear {{ $name }},</p>
<p>Thank you for your interest in our transportation services. We would like to confirm the following details:</p>

<div class="table-wrapper">
    <table>
        <tr>
            <th>Current or Former:</th>
            <td>{{ $current_former }}</td>
        </tr>
        <tr>
            <th>Raider #:</th>
            <td>{{ $raider_id }}</td>
        </tr>
        <tr>
            <th>Type of Player:</th>
            <td>{{ $type_player }}</td>
        </tr>
        <tr>
            <th>Name:</th>
            <td>{{ $name }}</td>
        </tr>
        <tr>
            <th>Phone:</th>
            <td>{{ $phone }}</td>
        </tr>
        <tr>
            <th>Email:</th>
            <td>{{ $email }}</td>
        </tr>
        <tr>
            <th>From:</th>
            <td>{{ $transport_from }}</td>
        </tr>
        <tr>
            <th>To:</th>
            <td>{{ $transport_to }}</td>
        </tr>
        <tr>
            <th>Transport Type:</th>
            <td>{{ $transport_type }}</td>
        </tr>
        <tr>
            <th>Vehicle Year:</th>
            <td>{{ $vehicle_year }}</td>
        </tr>
        <tr>
            <th>Vehicle Make:</th>
            <td>{{ $vehicle_make }}</td>
        </tr>
        <tr>
            <th>Vehicle Model:</th>
            <td>{{ $vehicle_model }}</td>
        </tr>
        <tr>
            <th>Is Operable:</th>
            <td>{{ $is_operable ? 'Yes' : 'No' }}</td>
        </tr>
        <tr>
            <th>Desired Pickup Date:</th>
            <td>{{ $pickup_date }}</td>
        </tr>
        <tr>
            <th>Desired Delivery Date:</th>
            <td>{{ $desired_date }}</td>
        </tr>
    </table>
</div>

<p>If you have any questions or would like to reach out to us directly feel free to contact us at <a href="tel:786-802-2080">786-802-2080</a> 9 AM - 7 PM EST or email us at <a href="mailto:daniel@dynamicautomovers.com">daniel@dynamicautomovers.com</a> anytime.</p>
<p>Please note we will be sending you a personalized quote for the information provided very soon. Thank you!</p>

</body>
</html>
