<x-main-layout>
<div class="container mx-auto p-4">
    <div class="flex flex-col p-6">
        <p class="font-light mb-10">Overwhelmed with a large array of quotes ranging from all sorts of different prices? Weary of so many companies calling you and offering you the world? We are aware of how flustering the quote receiving process can be so our job is to take that stress of your hands and worries of your mind. Our goal is to make this as simple a process for our customers so transporting your vehicle can become a care-free ordeal. We have put together a 10 step process so that you can understand exactly what goes into the process of transporting a car and can be on the same page as us every step of the way!</p>
        <h5 class="font-medium mb-5">1. CHOOSE US!</h5>
        <p class="font-light mb-10">Once you have decided to use DAM as your preferred vehicle shipper there are two ways to schedule a pick-up: Online or calling our direct line and scheduling your shipment with your assigned representative.</p>
        <h5 class="font-medium mb-5">2. CARRIER SEARCH</h5>
        <p class="font-light mb-10">The priority is to meet our customer’s needs, so we take down your desired transport date as well as your pickup and delivery address so we can properly search for the driver that will most suit your particular move. We take pride in not asking our customers for a down payment or deposit at this point like our competitors do, we do not get paid until our job is done!</p>
        <h5 class="font-medium mb-5">3. CHOOSING A CARRIER</h5>
        <p class="font-light mb-10">Your transport organizer will call you to present the different options he has researched so both you and him can decide on the one that will most suit your needs.</p>
        <h5 class="font-medium mb-5">4. BOOKING THE SPOT</h5>
        <p class="font-light mb-10">Once a carrier has been designated a spot will be booked and two emails will be sent to you. The first one is the vehicle dispatch sheet which contains all the information related to your move. We want you to feel comfortable and thus provide the carrier’s name and number, the pick-up and delivery dates that carrier has committed to, along with the total price that the trucker has signed for therefore eliminating the fear of truckers showing up and asking for more money. The second email is the company agreement that can easily be completed electronically.​</p>
        <h5 class="font-medium mb-5">5. SCHEDULING A TIME</h5>
        <p class="text-base font-light mb-5">The day before the pick up, the carrier will call you to provide an estimated window of time for arrival. This will be followed up with another call when the driver is one hour away. This way, you can continue with your day as planned without having to wait around all day.</p>
        <h5 class="font-medium mb-5">6. PICK UP PROCESS</h5>
        <p class="text-base font-light mb-5">The pick-up process is easy. You and the driver will perform a full inspection of the vehicle to note any scratches or dents. This is important for your protection against any potential damage that may occur during transport, as well as for the carrier to ensure that no damage was induced during transport. The bill of lading will then be signed by both parties, and the driver will safely load the car onto the carrier and be on their way.</p>
        <h5 class="font-medium mb-5">7. SCHEDULING DELIVERY</h5>
        <p class="text-base font-light mb-5">The delivery process is the same as the pick-up process. Within 24 hours of delivery, the driver will contact you to schedule a convenient window for delivery. The driver will again call you an hour before delivery to confirm the time.</p>
        <h5 class="font-medium mb-5">8. DELIVERY PROCESS</h5>
        <p class="text-base font-light mb-5">The driver will unload the vehicle, and both parties will inspect the car. Please be thorough to avoid any misunderstandings.</p>
        <h5 class="font-medium mb-5">9. TELL US HOW YOUR EXPERIENCE WAS</h5>
        <p class="text-base font-light mb-5">We encourage you to provide feedback on your experience with us, whether good or bad. If we did a good job, let us know. If there's anything we could have done better, please let us know. We are always striving to improve and the best way to do so is through our customers' opinions.</p>
        <h5 class="font-medium mb-5">10. SAVE OUR CONTACT INFO</h5>
        <p class="text-base font-light mb-5">Were you happy with our service? Did you enjoy working with your personal representative? Save our number and email! Our goal is to build lifelong relationships with our customers so that we can be your transporter of choice. Whether you're a dealer, car collector, student, worker, or a snowbird, we want to be the company you work with. Call us back directly to get the best rates and premium customer service that we are known for.</p>
    </div>
</div>
</x-main-layout>
