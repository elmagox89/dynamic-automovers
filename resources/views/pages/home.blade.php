<x-main-layout>
    <section class="flex h-screen w-full justify-center items-center bg-gray-300" style="background-image: url({{ asset('images/dynamic-auto-transport-service.webp') }}); background-size: cover; background-position: center;">
        @livewire('main-quote-form')
    </section>

    <div class="bg-white py-24 sm:py-32">
        <div class="mx-auto container px-6 lg:px-8">
            <h2 class="mx-auto max-w-4xl text-center text-3xl font-bold tracking-tight sm:text-4xl">COMPREHENSIVE AND RELIABLE SERVICES</h2>
            <p class="mx-auto mt-2 max-w-4xl text-center text-lg leading-8 text-gray-500">Experience our comprehensive and reliable vehicle and auto shipping services. We are committed to providing competitive prices with no upfront or additional fees. Our top-rated drivers ensure safe and fast delivery, backed by insurance and bonding.</p>
            <div class="flex justify-center mt-6">
                <button class="items-center bg-sky-700 hover:bg-sky-800 text-white font-bold py-2 px-4 rounded">
                    <a href="https://www.transportreviews.com/Company/Dynamic-Auto-Movers" target="_blank">CHECK OUT OUR REVIEWS</a>
                </button>
            </div>
            <div class="mt-20 flow-root">
                <div class="isolate -mt-16 grid max-w-md min-w-max grid-cols-1 md:grid-cols-2 gap-y-12 divide-y divide-gray-200 sm:mx-auto lg:-mx-4 lg:mt-0 lg:max-w-none lg:grid-cols-4 lg:divide-x lg:divide-y-0 xl:-mx-2">
                    <div class="pt-8 lg:px-8 lg:pt-0 xl:px-14">
                        <h3 id="tier-basic" class="text-base font-semibold leading-7 text-gray-900">OUR COMMITMENT</h3>
                        <ul role="list" class="mt-6 space-y-3 text-sm leading-6 text-gray-600">
                            <li class="flex gap-x-3">
                                <svg class="h-6 w-5 flex-none text-sky-700" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd" />
                                </svg>
                                Competitive Prices
                            </li>
                            <li class="flex gap-x-3">
                                <svg class="h-6 w-5 flex-none text-sky-700" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd" />
                                </svg>
                                No Upfront/Additional Fees
                            </li>
                            <li class="flex gap-x-3">
                                <svg class="h-6 w-5 flex-none text-sky-700" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd" />
                                </svg>
                                Top Rated Drivers
                            </li>
                        </ul>
                    </div>
                    <div class="pt-8 lg:px-8 lg:pt-0 xl:px-14">
                        <h3 id="tier-essential" class="text-base font-semibold leading-7 text-gray-900">COMPREHENSIVE SERVICES</h3>
                        <ul role="list" class="mt-6 space-y-3 text-sm leading-6 text-gray-600">
                            <li class="flex gap-x-3">
                                <svg class="h-6 w-5 flex-none text-sky-700" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd" />
                                </svg>
                                Insured and Bonded
                            </li>
                            <li class="flex gap-x-3">
                                <svg class="h-6 w-5 flex-none text-sky-700" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd" />
                                </svg>
                                Safe and Fast
                            </li>
                            <li class="flex gap-x-3">
                                <svg class="h-6 w-5 flex-none text-sky-700" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd" />
                                </svg>
                                No Payment Upfront
                            </li>
                        </ul>
                    </div>
                    <div class="pt-8 lg:px-8 lg:pt-0 xl:px-14">
                        <h3 id="tier-essential" class="text-base font-semibold leading-7 text-gray-900">KEYS TO SUCCESS</h3>
                        <ul role="list" class="mt-6 space-y-3 text-sm leading-6 text-gray-600">
                            <li class="flex gap-x-3">
                                <svg class="h-6 w-5 flex-none text-sky-700" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd" />
                                </svg>
                                Services
                            </li>
                            <li class="flex gap-x-3">
                                <svg class="h-6 w-5 flex-none text-sky-700" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd" />
                                </svg>
                                Values
                            </li>
                            <li class="flex gap-x-3">
                                <svg class="h-6 w-5 flex-none text-sky-700" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd" />
                                </svg>
                                History
                            </li>
                        </ul>
                    </div>
                    <div class="pt-8 lg:px-8 lg:pt-0 xl:px-14">
                        <h3 id="tier-essential" class="text-base font-semibold leading-7 text-gray-900">VEHICLE & AUTO SHIPPING</h3>
                        <ul role="list" class="mt-6 space-y-3 text-sm leading-6 text-gray-600">
                            <li class="flex gap-x-3">
                                <svg class="h-6 w-5 flex-none text-sky-700" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd" />
                                </svg>
                                Reduced Per Unit Cost
                            </li>
                            <li class="flex gap-x-3">
                                <svg class="h-6 w-5 flex-none text-sky-700" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd" />
                                </svg>
                                Shortened Transit Times
                            </li>
                            <li class="flex gap-x-3">
                                <svg class="h-6 w-5 flex-none text-sky-700" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd" />
                                </svg>
                                Variety and Experience
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="flex flex-col sm:flex-row bg-sky-700 text-white">
        <div class="w-full sm:w-1/2">
            <img class="h-[calc(100vh-40rem)] w-full object-cover" src="{{ asset('images/auto-transport-service.webp') }}" alt="">
        </div>
        <div class="w-full sm:w-1/3 lg:w-2/3 p-8 flex items-center">
            <div>
                <h4 class="text-2xl font-bold">OUR CLIENTS HAVE THE RIGHT TO EXPECT THE HIGHEST LEVEL OF SERVICE.</h4>
                <p class="text-base font-light text-xl text-white">When you trust us with the on-time delivery of your vehicle Dynamic Auto Movers deals with the big and small so you can have peace of mind.</p>
            </div>
        </div>
    </div>
    <div class="py-12 sm:py-32">
        <div class="mx-auto max-w-7xl px-6 lg:px-8">
            <div class="mx-auto max-w-2xl lg:max-w-none">
                <h2 class="mx-auto max-w-2xl text-center text-3xl font-bold tracking-tight sm:text-4xl">CREDENTIALS</h2>
                <p class="mx-auto mt-2 max-w-xl text-center text-lg leading-8 text-gray-500">Trusted by thousands of satisfied customers, our company stands as a symbol of reliability and excellence in vehicle transport.</p>
                <div class="mx-auto mt-10 grid grid-cols-2 sm:grid-cols-4 gap-x-8 gap-y-10 lg:mx-0">
                    <div class="flex flex-col text-center">
                        <a href="https://www.google.com/search?q=dynamic+auto+movers" target="_blank">
                            <img class="max-h-20 w-full object-contain" src="{{asset('/uploads/2022/03/580b57fcd9996e24bc43c51f.png')}}" alt="Rated on Google">
                        </a>
                        <h4 class="text-sky-700 my-4">Rated on Google</h4>
                    </div>
                    <div class="flex flex-col text-center">
                        <a href="https://www.transportreviews.com/Company/Dynamic-Auto-Movers" target="_blank">
                            <img class="max-h-20 w-full object-contain" src="https://api.movingsites.com/Image/Company5StarReviews/6814" alt="5 star rated on Transport Reviews">
                        </a>
                        <h4 class="text-sky-700 my-4">5 star rated on Transport Reviews</h4>
                    </div>
                    <div class="flex flex-col text-center">
                        <a href="https://www.yelp.com/biz/dynamic-auto-movers-miami" target="_blank">
                            <img class="max-h-20 w-full object-contain" src="{{ asset('images/yelp.webp') }}" alt="Rated with Yelp">
                        </a>
                        <h4 class="text-sky-700 my-4">Rated with Yelp</h4>
                    </div>
                    <div class="flex flex-col text-center">
                        <a href="https://www.bbb.org/us/fl/miami/profile/relocation-services/dynamic-auto-movers-llc-0633-90125570" target="_blank">
                            <img class="max-h-20 w-full object-contain" src="{{ asset('/uploads/2022/03/bbb-1.jpg') }}" alt="Rated with BBB">
                        </a>
                        <h4 class="text-sky-700 my-4">Rated with BBB</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-gray-100 py-20">
        <div class="mx-auto px-6 mb-12 lg:px-8">
            <div class="mx-auto max-w-[1200px] text-center py-2 mb-8">
                <h2 class="mx-auto max-w-2xl text-center text-3xl font-bold tracking-tight sm:text-4xl">FOLLOW ON INSTAGRAM</h2>
                <p class="mx-auto mt-2 max-w-xl text-center text-lg leading-8 text-gray-500">Follow us on Instagram, Facebook, and Twitter for the latest deals and expert tips. Let's enhance your transport experience together!</p>
            </div>
            <div class="mx-auto grid auto-rows-fr grid-cols-1 gap-8 lg:mx-0 lg:max-w-none md:grid-cols-2 lg:grid-cols-3" id="instagram"></div>
        </div>
    </div>
    <div class="flex justify-center p-6 text-sky-700 bg-gray-100">
        <a href="https://www.instagram.com/dynamicautomovers_/" target="_blank" rel="noopener noreferrer" class="flex items-center gap-2 bg-gradient-to-r from-purple-500 via-pink-500 to-red-500 hover:bg-sky-800 text-white font-bold py-2 px-4 rounded-sm">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                <path d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z"/>
            </svg>
            <span>Follow us @dynamicautomovers_</span>
        </a>
    </div>
</x-main-layout>

