<x-main-layout>
    <section class="flex h-screen w-full justify-center items-center bg-gray-300">
        @livewire('n-f-l-p-a-quote-form')
    </section>
</x-main-layout>
