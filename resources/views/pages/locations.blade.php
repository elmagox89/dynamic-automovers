<x-main-layout>
    <div class="container mx-auto p-4">
        <div class="flex flex-wrap">
            <div class="w-full lg:w-1/2 mb-4 lg:mb-0">
                <iframe title="Google Map" frameborder="0" style="width:100%;min-height:400px" src="https://www.google.com/maps?q=Dynamic%20Auto%20Movers&amp;output=embed&amp;hl=en&amp;z=12"></iframe>
            </div>
            <div class="w-full lg:w-1/2 px-4">
                <h4 class="font-medium text-xl mb-2"><strong>Dynamic Auto Movers</strong></h4>
                <p><strong>13362 SW 128 ST, Miami, FL 33186</strong></p>
                <p>P: 888-210-9906</p>
                <p>E: <a href="mailto:support@dynamicautomovers.com">support@dynamicautomovers.com</a></p>
                <p>H: Monday – Friday: 9:00 AM to 5:00 PM</p>
            </div>
        </div>

    </div>
</x-main-layout>
