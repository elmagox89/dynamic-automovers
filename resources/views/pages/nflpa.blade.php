<x-main-layout>
    <div class="container mx-auto p-4 text-gray-700">
        <div class="mx-auto my-8">
            <img src="{{ asset('uploads/2022/03/nflpa-dya-1.png') }}" alt="Dynamic Auto Movers logo" class="mx-auto mb-4">
            <p class="my-4 text-primary mt-5 font-bold underline bg-gradient-to-tr from-sky-800 to-blue-900 text-white p-6 rounded-lg shadow-md w-full max-w-6xl"><a class="text-primary font-bold" href="/quote-nflpa" target="_blank" rel="noreferrer noopener">Fill out your quote request information here</a></p>
            <hr class="my-8 border-t-2 border-gray-300">
            <p class="my-4 font-bold">At Dynamic Auto Movers we are happy and proud to begin working with the NFLPA for former and current players. Special discounts will apply for every auto, boat, or watercraft transport in the USA. We can accommodate your transportation needs for any route.</p>
            <p class="my-4 text-primary font-bold  bg-gradient-to-tr from-sky-800 to-blue-900 text-white p-6 rounded-lg shadow-md w-full max-w-6xl">All NFLPA members receive 20% on their transport move anywhere in the USA, including Hawaii and Alaska.</p>
            <p class="my-4">From pickup to delivery, Dynamic Auto Movers is committed to providing NFLPA members with outstanding customer services and valuable transportation solutions. Dynamic Auto only uses DOT Licensed carriers who are insured and have a 98% rating or higher.</p>
            <p class="my-4">Here are some of the many services Dynamic provides to all NFLPA members:</p>
            <ul class="list-disc list-inside my-4">
                <li>Open Transport</li>
                <li>Enclosed Transport</li>
                <li>Heavy-Hauling Equipment Transportation</li>
                <li>Boat, Motorcycle, and Recreational Vehicle Shipping</li>
                <li>Classic, Sports, and Exotic Vehicle Transport</li>
            </ul>
            <p class="my-4">During your move (before or after delivery), please be sure to take a photo w/ your vehicle and tag&nbsp;<strong>@dynamicautomovers_</strong>&nbsp;and&nbsp;<strong>@nflpa</strong>&nbsp;on all social media outlets.</p>
            <p class="my-4 font-bold">No Money Up Front</p>
            <p class="my-4">Pay on delivery no need to put down a credit card. Payment could be done on delivery.</p>
            <p class="my-4 font-bold">Personal Representative</p>
            <p class="my-4">When you book your order with Dynamic, you will be assigned a personal representative that will deal with you from beginning to end. We know communication is key, so you’ll be able to text, e-mail, or call this representative at any time.</p>
            <p class="my-4 font-bold">Relationship With Our Drivers</p>
            <p class="my-4">We understand the value of a customer’s car that is why we feel the need to only deal with drivers that we have formed a professional relationship with during our years in the industry. All of our carriers are fully licensed DOT drivers who have a 98% rating or higher. Proof of insurance will also be provided once a reservation is made.</p>
            <p class="my-4 font-bold">5 Star Rated Company</p>
            <p class="my-4">At the end of the day, we let our customers do the talking for us. Feel free to check out our reviews and ratings. You’ll see there are over 700 reviews on google and an A BBB rating and accreditation.</p>
            <p class="my-4 font-bold">To Begin The Process, It’s Simple.</p>
            <p class="my-4 text-primary font-bold underline bg-gradient-to-tr from-sky-800 to-blue-900 text-white p-6 rounded-lg shadow-md w-full max-w-6xl"><a class="text-primary font-bold" href="/quote-nflpa" target="_blank" rel="noreferrer noopener">Fill out your quote request information here</a></p>
            <p class="my-4">A representative will then reach out to you to give you a price and begin the process. We are looking forward to transporting your vehicle!</p>
            <p class="my-4 text-primary font-bold">Dynamic Auto Movers</p>
            <p class="my-4 text-primary font-bold">888-210-9906</p>
        </div>
    </div>
</x-main-layout>
