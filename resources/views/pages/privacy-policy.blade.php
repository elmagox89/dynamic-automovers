<x-main-layout>
    <div class="container mx-auto p-4">
        <div class="flex flex-col items-center p-6">
            <div class="p-4 border-2 border-gray-400 rounded">
                <h1 class="text-2xl font-bold">PRIVACY POLICY</h1>
                <p class="text-base font-medium">
                    We are committed to communicating with you in a professional manner and protecting your confidential information. We use the information you provide (e.g. name, address, phone number, email, agreement to receive newsletter/sales offers etc.) to contact you to share information about our (products/services). We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request. This company does not sell, trade or rent your personal information to others.
                </p>
                <p class="text-base font-medium">
                    Unless you ask us not to, we may contact you via email in the future with news about our company or information about our services. If you subscribe to our newsletter, you may unsubscribe at any time, either by using the unsubscribe link on the newsletter or contacting us via the email address given on our website.
                </p>
                <p class="text-base font-medium">
                    Please contact us at <a href="mailto:support@dynamicautomovers.com" class="font-medium underline">support@dynamicautomovers.com</a> to correct or update information at any time. (provide form or phone number)
                </p>
                <h2 class="text-xl font-bold mt-4">LOGGING AND COOKIES</h2>
                <p class="text-base font-medium">
                    When you browse our website, the following information about your visit is collected and stored: the computer’s Internet Protocol (IP) address (a number automatically assigned to your computer when you access the Internet); the domain from which you access the Internet (for example: aol.com, for an America Online account), the website address from which you came to our site (for example: www.google.com, if you came by clicking a link from a Google search); the date and time you arrived at our site and how long you spent here; the name and version of your computer’s operating system and browser Example: Windows 7/Internet Explorer 8.0; the pages you visited.
                </p>
                <p class="text-base font-medium">
                    We also use “cookies” on this site. A cookie is a piece of data stored on a site visitor’s hard drive to help us further determine usage of our site and sources of site traffic, improve access to our site and identify repeat visitors.
                </p>
                <h2 class="text-xl font-bold mt-4">LINKS</h2>
                <p class="text-base font-medium">
                    This website may contain links to other sites. Please be aware that we are not responsible for the content or privacy practices of such other sites.
                </p>
            </div>
        </div>
    </div>
</x-main-layout>
