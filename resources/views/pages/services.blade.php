<x-main-layout>
    <div class="container mx-auto p-4">
        <div class="grid grid-cols-1 gap-4 md:flex-row justify-center p-6 text-sky-700 max-w-7xl mx-auto">
            <div class="aspect-w-16 aspect-h-9">
                <iframe class="inset-0" src="https://player.vimeo.com/video/583561242?autoplay=0&amp;title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;controls=1&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
        <div class="flex flex-col items-center p-6">
            <div class="w-full  mb-8 md:mb-4">
                <h4 class="text-tertiary text-lg font-medium">OPEN TRANSPORT:</h4>
                <p class="text-gray-700">Standard method of transport for the industry. 90% of transports are processed this way including our dealership to dealership transfers. Safe and secure, vehicles are strapped with soft ties on our top of the line carriers and not moved throughout the process.</p>
            </div>

            <div class="w-full mb-8 md:mb-4">
                <h4 class="text-tertiary text-lg font-medium">ENCLOSED TRANSPORT:</h4>
                <p class="text-gray-700">The red carpet of transports. We provide hard side enclosed trucks with lift gates so your vehicle receives the treatment it deserves. Cars will be delivered in the same exact condition they were picked up in. Factors such as dirt, dust and debris are completely nullified.</p>
            </div>

            <div class="w-full mb-8 md:mb-4">
                <h4 class="text-tertiary text-lg font-medium">EXOTIC CAR TRANSPORT:</h4>
                <p class="text-gray-700">We are professional transporters of exotic car transports. Insured, bonded and reliable, we are capable of shipping any type of exotic vehicle door to door.</p>
            </div>

            <div class="w-full mb-8 md:mb-4">
                <h4 class="text-tertiary text-lg font-medium">CLASSIC CAR TRANSPORT:</h4>
                <p class="text-gray-700">We have been hauling antiques for years and have a large network of classic car collectors as clients. Our drivers are aware of the importance and value of such vehicles and handle them with extreme care.</p>
            </div>

            <div class="w-full mb-8 md:mb-4">
                <h4 class="text-tertiary text-lg font-medium">MOTORCYCLE TRANSPORTATION:</h4>
                <p class="text-gray-700">We have a dedicated team specifically for motorcycle transportation, this includes: Snowmobile, Jet-Ski and ATV. We have a specialized shipping system designed to protect your vehicle.</p>
            </div>

            <div class="w-full mb-8 md:mb-4">
                <h4 class="text-tertiary text-lg font-medium">BOAT TRANSPORT:</h4>
                <p class="text-gray-700">We can transport: Sailboats, speedboats, yachts and any other type of aqua vehicle.</p>
            </div>

            <div class="w-full mb-8 md:mb-4">
                <h4 class="text-tertiary text-lg font-medium">HEAVY HAULING EQUIPMENT:</h4>
                <p class="text-gray-700">Whether it be tractors, bulldozers, tow trucks or any other unique or oversized vehicle we can transport it.</p>
            </div>
        </div>
    </div>
</x-main-layout>
