<x-main-layout>
    <section class="flex h-screen w-full justify-center items-center bg-gray-300">
        @livewire('main-quote-form')
    </section>
</x-main-layout>
