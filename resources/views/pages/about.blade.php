<x-main-layout>
    <div class="container mx-auto p-4">
        <div class="flex flex-col p-6">

            <h2 class="text-xl font-bold text-gray-700 mb-4">AUTO TRANSPORT</h2>
            <div class="flex flex-col sm:flex-row">
                <div class="w-full sm:w-1/3">
                    <img class="h-80 w-full object-cover" src="{{asset('/uploads/2022/03/services-1.jpg')}}" alt="">

                </div>
                <div class="w-full sm:w-2/3 p-8 flex items-center">
                    <div>
                        <h4 class="text-xl text-gray-600 font-bold">WE’VE BUILT OUR REPUTATION ON EXCELLENCE IN SHIPPING AND LOGISTICS, WITH A CONSTANT FOCUS ON SERVING OUR CUSTOMERS.</h4>
                        <p class="text-sm text-gray-600 mb-4">When it comes to auto transport we know effective and fast communication is key to client comfort. We provide same day response to your inquiries, from pick-up to delivery, we’ll be there every step of the way.</p>
                        <p class="text-sm text-gray-600 mb-4">You trust us to ship your vehicle, you’ll know that the safe transport of your vehicle is in the hands of skilled, qualified professionals. We only use DOT Licensed drivers who are insured and have a 98% rating or higher.</p>
                        <p class="text-sm text-gray-600 font-bold mb-4">NO TRUCK TRANSFER, YOUR VEHICLE WILL always STAY ON THE SAME CARRIER.</p>
                    </div>
                </div>
            </div>
            <hr>
            <div class="mt-2 md:mt-6">
                <h2 class="text-xl font-bold text-gray-700 mb-4">WHY CHOOSE US?</h2>
                <div class="w-full md:w-2/3 mb-8">
                    <div class="mb-4">
                        <h3 class="text-lg font-bold text-gray-700 mb-2">No Money Up Front</h3>
                        <p class="text-gray-600 text-sm">Many companies preach the fact that they do not charge money upfront yet the fact is that when you attempt to book an order they will ask you for a credit or debit card to take a deposit or down payment.</p>
                    </div>
                    <div class="mb-4">
                        <h3 class="text-lg font-bold text-gray-700 mb-2">Personal Representative</h3>
                        <p class="text-gray-600 text-sm">When you book your order with Dynamic, you will be assigned a personal representative that will deal with you from beginning to end.</p>
                    </div>
                    <div class="mb-4">
                        <h3 class="text-lg font-bold text-gray-700 mb-2">Relationship With Our Drivers</h3>
                        <p class="text-gray-600 text-sm">We understand the value of a customer’s car that is why we feel the need to only deal with drivers that we have formed a professional relationship with during our years in the industry.</p>
                    </div>
                    <div class="mb-4">
                        <h3 class="text-lg font-bold text-gray-700 mb-2">5 Star Rated Company</h3>
                        <p class="text-gray-600 text-sm">At the end of the day we let our customers do the talking for us.</p>
                    </div>
                </div>
            </div>
            <hr>
            <div class="mt-2 md:mt-6">
                <h2 class="text-xl font-bold text-gray-700 mb-4">LATEST NEWS</h2>
                <div class="flex flex-col md:flex-row mb-8">
                    <div class="w-full md:w-1/2 mb-4 md:mb-0">
                        <p class="text-sm text-gray-600 font-bold mb-2">ADVANCED SOFTWARE</p>
                        <p class="text-sm text-gray-600">At a time when other logistics companies are scaling down, we’re betting on the future with latest software and equipment that make us the best choice to be your transportation partner.</p>
                    </div>
                    <div class="w-full md:w-1/2 mb-4 md:mb-0">
                        <p class="text-sm text-gray-600 font-bold mb-2">OUR WORKFORCE IS NOW BETTER</p>
                        <p class="text-sm text-gray-600">We’ve implemented a new training system for all employees that includes cross training in all relevant areas of the company.</p>
                    </div>
                </div>
            </div>
            <hr>
            <div class="mt-2 md:mt-6">
                <h2 class="text-xl font-bold text-gray-700 mb-4">WE ACCEPT</h2>
                <div class="w-full md:w-2/3 mb-8">
                    <img src="{{asset('/uploads/2022/03/credit-cards-1.png')}}" alt="Accepted payment methods">
                </div>
            </div>
        </div>
    </div>
</x-main-layout>
