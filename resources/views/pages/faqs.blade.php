<x-main-layout>
    <div class="container mx-auto p-4">
        <div class="flex flex-col p-6">
            <div class="w-full">
                <div id="accordion-open" data-accordion="close">
                    <h2 id="accordion-open-heading-1">
                        <button type="button"
                                class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 rounded-t-xl focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                                data-accordion-target="#accordion-open-body-1" aria-expanded="true"
                                aria-controls="accordion-open-body-1">
                            <span class="flex items-center">How soon can you pick up my car?</span>
                            <svg data-accordion-icon class="w-6 h-6 rotate-180 shrink-0" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M5. 293 7. 293a1 1 0 011.414 0L10 10.586l3. 293-3. 293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                    </h2>
                    <div id="accordion-open-body-1" class="hidden" aria-labelledby="accordion-open-heading-1">
                        <div
                            class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="mb-2 text-gray-500 dark:text-gray-400">In most routes we are able to have a driver
                                pick up a car within 48 hours of booking a reservation. If for any reason your vehicle
                                is a little bit off the beaten path we might ask for a few more days cushion just in
                                case.</p>
                        </div>
                    </div>
                    <h2 id="accordion-open-heading-2">
                        <button type="button"
                                class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                                data-accordion-target="#accordion-open-body-2" aria-expanded="true"
                                aria-controls="accordion-open-body-2">
                            <span class="flex items-center">How long will my transport take?</span>
                            <svg data-accordion-icon class="w-6 h-6 rotate-180 shrink-0" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M5. 293 7. 293a1 1 0 011.414 0L10 10.586l3. 293-3. 293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                    </h2>
                    <div id="accordion-open-body-2" class="hidden" aria-labelledby="accordion-open-heading-2">
                        <div
                            class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="mb-2 text-gray-500 dark:text-gray-400">Drivers drive around 600 miles a day,
                                however keep in mind that loading and unloading vehicles does take time as well. As a
                                general rule any cross country move (from coast to coast) is generally around 7 days
                                while any transport from North to South is from 3-5 days. To find out about how long
                                your exact route will take please ask your designated representative.</p>
                        </div>
                    </div>
                    <h2 id="accordion-open-heading-3">
                        <button type="button"
                                class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                                data-accordion-target="#accordion-open-body-3" aria-expanded="true"
                                aria-controls="accordion-open-body-3">
                            <span class="flex items-center">Is my car insured?</span>
                            <svg data-accordion-icon class="w-6 h-6 rotate-180 shrink-0" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M5. 293 7. 293a1 1 0 011.414 0L10 10.586l3. 293-3. 293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                    </h2>
                    <div id="accordion-open-body-3" class="hidden" aria-labelledby="accordion-open-heading-3">
                        <div
                            class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="mb-2 text-gray-500 dark:text-gray-400">Absolutely, all our truckers are fully
                                insured bumper to bumper with no deductible for the customer. Drivers will perform a
                                full inspection of the vehicle at pick up and also at delivery which will be signed by
                                both parties. Once a carrier is selected we would be more than happy to send you their
                                certificate of insurance.</p>
                        </div>
                    </div>
                    <h2 id="accordion-open-heading-4">
                        <button type="button"
                                class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                                data-accordion-target="#accordion-open-body-4" aria-expanded="true"
                                aria-controls="accordion-open-body-4">
                            <span class="flex items-center">Can I put items in my car?</span>
                            <svg data-accordion-icon class="w-6 h-6 rotate-180 shrink-0" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M5. 293 7. 293a1 1 0 011.414 0L10 10.586l3. 293-3. 293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                    </h2>
                    <div id="accordion-open-body-4" class="hidden" aria-labelledby="accordion-open-heading-4">
                        <div
                            class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="mb-2 text-gray-500 dark:text-gray-400">Technically auto carriers do not have a
                                license for carrying personal or household goods, that being said we are aware that
                                people are moving and do allow 100lbs of belongings to be put inside the vehicle. If you
                                need more for whatever reason please contact us as exceptions can be made.</p>
                        </div>
                    </div>
                    <h2 id="accordion-open-heading-5">
                        <button type="button"
                                class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                                data-accordion-target="#accordion-open-body-5" aria-expanded="true"
                                aria-controls="accordion-open-body-5">
                            <span class="flex items-center">What does my price include?</span>
                            <svg data-accordion-icon class="w-6 h-6 rotate-180 shrink-0" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M5. 293 7. 293a1 1 0 011.414 0L10 10.586l3. 293-3. 293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                    </h2>
                    <div id="accordion-open-body-5" class="hidden" aria-labelledby="accordion-open-heading-5">
                        <div
                            class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="mb-2 text-gray-500 dark:text-gray-400">The prices are all inclusive, that means
                                there are no additional fees, taxes or hidden costs. Things that go into the total
                                tariff are tolls, gas, insurance and door to door.</p>
                        </div>
                    </div>
                    <h2 id="accordion-open-heading-6">
                        <button type="button"
                                class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                                data-accordion-target="#accordion-open-body-6" aria-expanded="true"
                                aria-controls="accordion-open-body-6">
                            <span class="flex items-center">How does payment work?</span>
                            <svg data-accordion-icon class="w-6 h-6 rotate-180 shrink-0" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M5. 293 7. 293a1 1 0 011.414 0L10 10.586l3. 293-3. 293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                    </h2>
                    <div id="accordion-open-body-6" class="hidden" aria-labelledby="accordion-open-heading-6">
                        <div
                            class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="mb-2 text-gray-500 dark:text-gray-400">Our company takes pride in not charging our
                                customers up front; this means that there will be no payment before your car is
                                delivered. However payment is split into two: A small fee (usually around $150 depending
                                on the transport) will be charged on a credit card that we authorize once car is picked
                                up but don’t process until delivery, and the remaining balance is due to the carrier at
                                delivery via cash, cashier’s check or money order.</p>
                        </div>
                    </div>
                    <h2 id="accordion-open-heading-7">
                        <button type="button"
                                class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                                data-accordion-target="#accordion-open-body-7" aria-expanded="true"
                                aria-controls="accordion-open-body-7">
                            <span class="flex items-center">How does payment work?</span>
                            <svg data-accordion-icon class="w-6 h-6 rotate-180 shrink-0" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M5. 293 7. 293a1 1 0 011.414 0L10 10.586l3. 293-3. 293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                    </h2>
                    <div id="accordion-open-body-7" class="hidden" aria-labelledby="accordion-open-heading-7">
                        <div
                            class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="mb-2 text-gray-500 dark:text-gray-400">Our company takes pride in not charging our
                                customers up front; this means that there will be no payment before your car is
                                delivered. However payment is split into two: A small fee (usually around $150 depending
                                on the transport) will be charged on a credit card that we authorize once car is picked
                                up but don’t process until delivery, and the remaining balance is due to the carrier at
                                delivery via cash, cashier’s check or money order.</p>
                        </div>
                    </div>
                    <h2 id="accordion-open-heading-8">
                        <button type="button"
                                class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                                data-accordion-target="#accordion-open-body-8" aria-expanded="true"
                                aria-controls="accordion-open-body-8">
                            <span class="flex items-center">What happens if there is damage done to my vehicle?</span>
                            <svg data-accordion-icon class="w-6 h-6 rotate-180 shrink-0" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M5. 293 7. 293a1 1 0 011.414 0L10 10.586l3. 293-3. 293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                    </h2>
                    <div id="accordion-open-body-8" class="hidden" aria-labelledby="accordion-open-heading-8">
                        <div
                            class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="mb-2 text-gray-500 dark:text-gray-400">While this is a very uncommon thing in the
                                transport industry we always like to be prepared for any scenario. In the rare case
                                where any sort of damage is done to a vehicle the most important thing is to always note
                                it down on the bill of lading as this is would be the evidence that it occurred during
                                transit so a claim can be filed after.</p>
                        </div>
                    </div>
                    <h2 id="accordion-open-heading-9">
                        <button type="button"
                                class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200  focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                                data-accordion-target="#accordion-open-body-9" aria-expanded="true"
                                aria-controls="accordion-open-body-9">
                            <span class="flex items-center">What type of trucks do you use?</span>
                            <svg data-accordion-icon class="w-6 h-6 rotate-180 shrink-0" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M5. 293 7. 293a1 1 0 011.414 0L10 10.586l3. 293-3. 293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                    </h2>
                    <div id="accordion-open-body-9" class="hidden" aria-labelledby="accordion-open-heading-9">
                        <div
                            class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="mb-2 text-gray-500 dark:text-gray-400">We have a large variety of trucks from
                                small flatbeds for shorter routes to large double decker carriers for longer trips to
                                enclosed trucks for a more private experience. Depending on what sort of transport you
                                are looking for have the peace of mind that we will have the appropriate equipment for
                                you!</p>
                        </div>
                    </div>
                    <h2 id="accordion-open-heading-10">
                        <button type="button"
                                class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200  focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                                data-accordion-target="#accordion-open-body-10" aria-expanded="true"
                                aria-controls="accordion-open-body-10">
                            <span class="flex items-center">Will my car be moved from truck to truck?</span>
                            <svg data-accordion-icon class="w-6 h-6 rotate-180 shrink-0" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M5. 293 7. 293a1 1 0 011.414 0L10 10.586l3. 293-3. 293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                    </h2>
                    <div id="accordion-open-body-10" class="hidden" aria-labelledby="accordion-open-heading-10">
                        <div
                            class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="mb-2 text-gray-500 dark:text-gray-400">Absolutely not! We deal with one truck and
                                one trucker for all of our shipments. The same carrier that picks up your car will be
                                the one delivering your car. We do not partake in terminal moves either where your car
                                is dropped off and picked up by someone else. Your vehicle will not change trucks or
                                truckers; once your car is picked up it will be a straight shot to its destination.</p>
                        </div>
                    </div>
                    <h2 id="accordion-open-heading-11">
                        <button type="button"
                                class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200  focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                                data-accordion-target="#accordion-open-body-11" aria-expanded="true"
                                aria-controls="accordion-open-body-11">
                            <span class="flex items-center">How can I track my car?</span>
                            <svg data-accordion-icon class="w-6 h-6 rotate-180 shrink-0" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M5. 293 7. 293a1 1 0 011.414 0L10 10.586l3. 293-3. 293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                    </h2>
                    <div id="accordion-open-body-11" class="hidden" aria-labelledby="accordion-open-heading-11">
                        <div
                            class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="mb-2 text-gray-500 dark:text-gray-400">While some carriers do possess GPS tracking there are also many that don’t, but fear not, we will always provide you with the carrier’s phone number so you can check on the progress of your transportation and of course feel free to contact your representative or any other member of our staff for updates as well.</p>
                        </div>
                    </div>
                    <h2 id="accordion-open-heading-12">
                        <button type="button"
                                class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200  focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                                data-accordion-target="#accordion-open-body-12" aria-expanded="true"
                                aria-controls="accordion-open-body-12">
                            <span class="flex items-center">What time will the trucker be picking up my car?</span>
                            <svg data-accordion-icon class="w-6 h-6 rotate-180 shrink-0" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M5. 293 7. 293a1 1 0 011.414 0L10 10.586l3. 293-3. 293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                    </h2>
                    <div id="accordion-open-body-12" class="hidden" aria-labelledby="accordion-open-heading-12">
                        <div
                            class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="mb-2 text-gray-500 dark:text-gray-400">We are aware that as customers you always want things to go perfect as do we. However, we ask for your understanding as trucking is a hard business to pinpoint an exact time due to many factors. What we have found that works best is for our carriers to work out a window with the customer and then call them when they are about an hour away. We are also aware that some customers are working with certain deadlines and factors like flights come into play and we always do our best to prioritize our customer’s timelines and work around them.</p>
                        </div>
                    </div>
                    <h2 id="accordion-open-heading-13">
                        <button type="button"
                                class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-gray-200 rounded-b-xl focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                                data-accordion-target="#accordion-open-body-13" aria-expanded="true"
                                aria-controls="accordion-open-body-13">
                            <span class="flex items-center">I live in a narrow street, how will the trucker access my road?</span>
                            <svg data-accordion-icon class="w-6 h-6 rotate-180 shrink-0" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M5. 293 7. 293a1 1 0 011.414 0L10 10.586l3. 293-3. 293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </button>
                    </h2>
                    <div id="accordion-open-body-13" class="hidden" aria-labelledby="accordion-open-heading-13">
                        <div
                            class="p-5 font-light border border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="mb-2 text-gray-500 dark:text-gray-400">Most of our customers live in very residential streets and you will be surprised how close to your residence our carriers can get! In the case where the driver cannot get right to your front door he might ask you to meet him at a nearby street or parking lot where loading and unloading might be easier.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-main-layout>
